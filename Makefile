help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

jupytext: ## Convert JSON notebook to plain text
	jupytext --to py:sphinx **/*.ipynb

sphinx: ## Convert to sphinx gallery
	python ipynb_to_gallery.py tutorials/training_agents/FrozenLake_tuto.ipynb

# rst: ## Convert to reStructuredText
# 	jupyter nbconvert --to rst tutorials/FrozenLake_tuto.ipynb

copy: ## Copy files to Gymnasium
	cp -v tutorials/training_agents/FrozenLake_tuto.* /home/apierre/Dev/Farama-Foundation/Gymnasium/docs/tutorials/training_agents/.
	cp -v _static/img/tutorials/*.png /home/apierre/Dev/Farama-Foundation/Gymnasium/docs/_static/img/tutorials/.
